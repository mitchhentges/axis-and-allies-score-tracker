# [Axis & Allies Score Tracker](https://axisandallies.fuzzlesoft.ca/)

Missing your "score tracker" sheet? Hate bumping the table and having to reverse-engineer where in tarnation all the
countries are at? Do you just want to add a little modern flair to your A&A experience? Welcome to the officially
unofficial Axis & Allies Score Tracker (1940 2nd edition OEM Standard Regular 87 Octane);

This site makes it easy to track IPCs for each country:
* You can directly enter in IPCs/income into the text fields, or bump up/down using the `-`/`+` keys or your keyboard
  (mouse over the income `-`/`+` and `Collect` buttons to see hotkeys)
* Convenient `Undo` button when an "oops" was made
* The values are remembered if you leave the site and come back
* Incredibly witty and topical names for each country