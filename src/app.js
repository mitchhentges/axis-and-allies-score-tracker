const countries = ["germany", "sovietUnion", "japan", "us", "china", "ukAtlantic", "ukPacific", "italy", "anzac", "france"]
const keysChangeIncome = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"];
const keysCollectIncome = ["a", "s", "d", "f", "g", "h", "j", "k", "l", ";"]

class State {
    constructor(total, income, turnNumber) {
        this.total = total;
        this.income = income;
        this.turnNumber = turnNumber;
    }

    next(cb) {
        const total = Object.fromEntries(Object.entries(this.total));
        const income = Object.fromEntries(Object.entries(this.income));
        const state = new State(total, income, this.turnNumber);
        return cb(state) || state;
    }

    serialize() {
        return {
            total: this.total,
            income: this.income,
            turnNumber: this.turnNumber,
        }
    }

    static fromSerialized(obj) {
        return new State(obj.total, obj.income, obj.turnNumber);
    }

    static fromInitial() {
        return new State(
            {
                germany: 30,
                sovietUnion: 37,
                japan: 26,
                us: 52,
                china: 12,
                ukAtlantic: 28,
                ukPacific: 12,
                italy: 10,
                anzac: 10,
                france: 17
            },
            {
                germany: 30,
                sovietUnion: 37,
                japan: 26,
                us: 52,
                china: 12,
                ukAtlantic: 28,
                ukPacific: 12,
                italy: 10,
                anzac: 10,
                france: 17
            },
            1,
        )
    }
}

class TextFields {
    constructor() {
        const rawTotalFields = document.querySelectorAll("#total-ipcs input");
        const totalEntries = [];
        for (let i = 0; i < rawTotalFields.length; i++) {
            totalEntries.push([countries[i], rawTotalFields.item(i)]);
        }
        this.total = Object.fromEntries(totalEntries);

        const rawIncomeFields = document.querySelectorAll("#income input");
        const incomeEntries = [];
        for (let i = 0; i < rawIncomeFields.length; i++) {
            incomeEntries.push([countries[i], rawIncomeFields.item(i)]);
        }
        this.income = Object.fromEntries(incomeEntries);
        this.undoButton = document.querySelector("#undo");
        this.turnNumber = document.querySelector("#turn-number")
    }

    updateFromState(state, hasEarlierStates) {
        Object.entries(state.total).forEach(([country, value]) => {
            this.total[country].value = value;
        });
        Object.entries(state.income).forEach(([country, value]) => {
            this.income[country].value = value;
        });
        this.undoButton.disabled = !hasEarlierStates;
        this.turnNumber.innerHTML = "Turn " + state.turnNumber;
    }
}

function save(history) {
    const serializedHistory = history.map(state => state.serialize());
    window.localStorage.setItem("history", JSON.stringify(serializedHistory));
}

function load() {
    const serializedHistory = window.localStorage.getItem("history");
    if (!serializedHistory) {
        return;
    }

    return JSON.parse(serializedHistory).map(raw => State.fromSerialized(raw));
}

function main() {
    let history = load() || [State.fromInitial()];
    const textFields = new TextFields();
    const currentState = () => history[history.length - 1];
    const render = () => {
        textFields.updateFromState(currentState(), history.length > 1);
        save(history);
    }
    const tick = (cb) => {
        history.push(currentState().next(cb));
        render();
    }

    document.querySelectorAll("#total-ipcs td").forEach((td, i) => {
        if (i === 0) {
            return;
        }

        td.querySelector("button:first-child").addEventListener("click", () => {
            tick((state) => {
                state.total[countries[i-1]] -= 1;
            })
        });

        td.querySelector("button:last-child").addEventListener("click", () => {
            tick((state) => {
                state.total[countries[i-1]] += 1;
            })
        });

        td.querySelector("input").addEventListener("change", event => {
            tick(state => {
                const value = parseInt(event.target.value, 10);

                if (Number.isNaN(value)) {
                    return;
                }

                state.total[countries[i-1]] = value;
            })
        });
    });

    document.querySelectorAll("#income td").forEach((td, i) => {
        if (i === 0) {
            return;
        }

        let button = td.querySelector("button:first-child");
        button.title = keysChangeIncome[i-1]
        button.addEventListener("click", () => {
            tick((state) => {
                state.income[countries[i-1]] -= 1;
            })
        });

        button = td.querySelector("button:last-child")
        button.title = keysChangeIncome[i-1].toUpperCase();
        button.addEventListener("click", () => {
            tick((state) => {
                state.income[countries[i-1]] += 1;
            })
        });

        td.querySelector("input").addEventListener("change", event => {
            tick(state => {
                const value = parseInt(event.target.value, 10);

                if (Number.isNaN(value)) {
                    return;
                }

                state.income[countries[i-1]] = value;
            })
        });
    });

    document.querySelectorAll("#collect button").forEach((button, i) => {
        button.title = keysCollectIncome[i];
        button.addEventListener("click", () => {
            tick((state) => {
                state.total[countries[i]] += state.income[countries[i]];
                if (countries[i] === "france") {
                    state.turnNumber += 1;
                }
            })
        });
    });

    document.querySelector("#undo").addEventListener("click", () => {
        history.pop();
        render();
    });

    document.querySelector("#reset").addEventListener("click", () => {
        history = [State.fromInitial()];
        render();
    });

    document.addEventListener("keypress", event => {
        keysChangeIncome.forEach((key, i) => {
            if (key === event.key) {
                tick(state => {
                    state.income[countries[i]] -= 1;
                })
            } else if (key.toUpperCase() === event.key) {
                tick(state => {
                    state.income[countries[i]] += 1;
                })
            }
        });

        keysCollectIncome.forEach((key, i) => {
            if ([key, key.toUpperCase()].includes(event.key)) {
                tick(state => {
                    state.total[countries[i]] += state.income[countries[i]];
                    if (countries[i] === "france") {
                        state.turnNumber += 1;
                    }
                })
            }
        });
    })

    render();
}

document.addEventListener("DOMContentLoaded", main);